#include "t_vec.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

typedef struct t_vector
{
  size_t len;
  size_t capacity;
  size_t item_size;
  pthread_mutex_t lock;
  void *data;
} vec;

static const size_t DEFAULT_CAPACITY = 32;

// Returns a newly allocated Vector with the data pointer
// pointing to newly allocated space that is item_size * DEFAULT_CAPACITY
// bytes large
T_Vec *
T_Vec_init(size_t item_size)
{
    T_Vec *v = calloc(1, sizeof(vec));
    if (v == NULL)
    {
        perror("Unable to initialize vector.");
        return NULL;
    }
    void *vec_data = calloc(DEFAULT_CAPACITY, item_size);
    if (vec_data == NULL)
    {
        perror("Unable to initialize data storage for vector.");
        return NULL;
    }
    v->len = 0;
    v->capacity = DEFAULT_CAPACITY;
    v->item_size = item_size;
    v->data = vec_data;
    if (pthread_mutex_init(&v->lock, NULL) == -1)
    {
        perror("Unable to initialize mutex for T_Vec.");
        return NULL;
    }
    return v;
}

int
T_Vec_destroy(T_Vec *v)
{
    free(v->data);
    free(v);
    return EXIT_SUCCESS;
}

size_t
T_Vec_len(T_Vec *v)
{
    return v->len;
}

void *
T_Vec_start(T_Vec *v)
{
    return v->data;
}

int
T_Vec_push(T_Vec *v, void *push_data)
{
    if (v->len == v->capacity)
    {
        // t_vec_enlarge does its own locking/unlocking of the mutex
        int enlarged = t_vec_enlarge(v);
        if (enlarged == EXIT_FAILURE)
        {
            perror("Unable to resize Vector to hold new item.");
            return EXIT_FAILURE;
        }
    }
    // MUTEX LOCK //
    pthread_mutex_lock(&v->lock);
    memcpy(v->data + (v->item_size * v->len), push_data, v->item_size);
    v->len++;
    pthread_mutex_unlock(&v->lock);
    // MUTEX UNLOCK //
    return EXIT_SUCCESS;
}

void *
T_Vec_pop(T_Vec *v)
{
    void *item = calloc(1, v->item_size);
    if (item == NULL)
    {
        perror("Unable to allocate space for popped item.");
        return NULL;
    }
    void *pop_data = T_Vec_fetch_ref(v, v->len - 1); //v->data + (v->len * v->item_size);
    // Copy the data we're popping into the newly allocated space for item
    memcpy(item, pop_data, v->item_size);

    // MUTEX LOCK //
    pthread_mutex_lock(&v->lock);

    // Overwrite the data we just popped with 0s
    memset(pop_data, 0, v->item_size);

    // Decrement length
    v->len--;

    pthread_mutex_unlock(&v->lock);
    // MUTEX UNLOCK //
    return item;
}

// Returns a pointer to a newly allocated copy
// of the item at idx
void *
T_Vec_fetch_ref(T_Vec *v, size_t idx)
{
    if (idx > v->len)
    {
        return NULL;
    }
    return v->data + (idx * v->item_size);
}

int
T_Vec_map(T_Vec *v, void * map_arg, MapFunc map_func)
{
    for (size_t idx = 0; idx < v->len; ++idx)
    {
        void *item = T_Vec_fetch_ref(v, idx);
        if (map_func(item, map_arg))
        {
            return idx;
        }
    }
    return -1;
}

// This function takes a function that should
// return 0 upon a match criteria and anything
// else in every other circumstance.  It returns
// a pointer INSIDE the Vectors data field.
void *
T_Vec_find_ref(T_Vec *v, void *find_arg, FindFunc find_func)
{

    // Loop over all the items in the array
    for (size_t idx = 0; idx < v->len; ++idx)
    {
        // Pull out appropriate item
        void *item = T_Vec_fetch_ref(v, idx);

        // Apply find function
        if (!find_func(item, find_arg))
        {
            // If the function returned 0 indicating a match
            return item;
        }
    }
    // If we made it here there was no match.
    return NULL;
}

// This function is identical to Vec_find_ref except it
// returns the index of the found item, not a pointer
// to the item.  If no match is found it returns -1
ssize_t
T_Vec_find_idx(T_Vec *v, void *find_arg, FindFunc find_func)
{
    if (T_Vec_len(v) == 0)
    {
        return -1;
    }
    // Loop over all the items in the array
    for (size_t idx = 0; idx < v->len; ++idx)
    {
        // Pull out appropriate item
        void *item = T_Vec_fetch_ref(v, idx);

        // Apply find function
        if (!find_func(item, find_arg))
        {
            // If the function returned 0 indicating a match
            return idx;
        }
    }
    // If we made it here there was no match.
    return -1;
}

// This function removes item by index.  Likely to
// be done with Vec_find_idx.  This function has no
// way to fail.
int
T_Vec_remove(T_Vec *v, size_t idx)
{
    for (; idx > v->len; ++idx)
    {
        memcpy(T_Vec_fetch_ref(v, idx), T_Vec_fetch_ref(v, idx + 1), v->item_size);
    }
    v->len--;
    return EXIT_SUCCESS;
}

int
T_Vec_lock(T_Vec *v)
{
    if (!pthread_mutex_lock(&v->lock))
    {
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}

int
T_Vec_unlock(T_Vec *v)
{
    if (!pthread_mutex_unlock(&v->lock))
    {
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}

// Internal use only
int
t_vec_enlarge(T_Vec *v)
{

    void *new_data = realloc(v->data, 2 * v->capacity * v->item_size);
    if (new_data == NULL)
    {
        perror("Failed to realloc when enlarging Vector data.");
        return EXIT_FAILURE;
    }
    // MUTEX LOCK //
    pthread_mutex_lock(&v->lock);
    v->capacity *= 2;
    v->data = new_data;
    pthread_mutex_unlock(&v->lock);
    // MUTEX UNLOCK //
    return EXIT_SUCCESS;
}



