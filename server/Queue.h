//
// Created by charizard on 9/4/19.
//

#ifndef SERVER_QUEUE_H
#define SERVER_QUEUE_H

/** @file */


// For type Task
#include "Pool.h"

typedef struct queue Queue;

/**
 * Create a new Queue
 *
 * @return newly allocated Queue
 */
Queue *
Queue_create(void);

/**
 * Add a task to the Queue
 *
 * @param queue to enqueue to
 * @param task to add
 * @param argument to task
 * @return true if successfully added, false otherwise
 */
bool
Queue_enqueue(Queue *q, Task t, void *arg);

/**
 * Remove a task from the Queue
 *
 * @param Queue to dequeue from
 * @param task from Queue (output parameter)
 * @param argument for task from queue (output parameter)
 */
void
Queue_dequeue(Queue *q, Task *t, void **arg);

/**
 * Free up resources from Queue
 *
 * @param queue to destroy
 */
void
Queue_destroy(Queue *q);

#endif //SERVER_QUEUE_H
