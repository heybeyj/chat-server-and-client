//
// Created by charizard on 9/3/19.
//

#ifndef SERVER_USER_H
#define SERVER_USER_H

/** @file */

#include "t_vec.h"
#include "Channel.h"

#define USERNAME_LEN 16

// Holds information about a single user
typedef struct user
{
    char username[USERNAME_LEN];
    int sock_fd;
    struct user *next;
    struct channel *user_channels;
} user;

typedef struct user_info
{
    char username[USERNAME_LEN];
    int sock_fd;
    T_Vec *channels;
} user_info;

// Container for array of users and pollfds
// index X into user or pollfd should always correspond to the same user
typedef struct user_list
{
    size_t len;
    size_t capacity;
    user *userll;
    struct pollfd *pollfds;
} user_list;

/**
 * Create a new list of users
 * @return a newly allocated user_list
 */
user_list *
User_init();

/**
 * Removes a user from the user_list keyed by file descriptor of their connection
 * @param fd File descriptor to remove/close
 * @param users the user_list to remove from
 * @return 0 on success, 1 on failture
 */
int
User_remove(T_Vec *users, T_Vec *pollfds, T_Vec *channels, size_t user_idx, size_t pollfd_idx);

/**
 * Creates a newly allocated user and adds it to the user_list
 * @param username Name of the new user
 * @param users Pre-existing list to add the user to
 * @param fd File descriptor of their connection
 * @return 0 on success, 1 on failure
 */
int
User_add(const char *username, T_Vec *users, T_Vec *pollfds, int fd);

/**
 * Frees all resources associated with a user_list
 * @param users The user_list to destroy
 * @return 0 on success, 1 on failure
 */
int
User_destroy(user_list *users);

int
get_user_by_name(void* arg_user, void *arg_name);

int
get_channel_by_name(void *arg_channel, void *arg_channel_name);

#endif //SERVER_USER_H
