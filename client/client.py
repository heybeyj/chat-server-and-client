#!/usr/bin/env python3
"""This module implements the SHIRC (SHitty IRC) client in its entirety

Copyright (c) 2019 Christopher Charland

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

# Disable warnings for TODOs because its annoying.
# pylint: disable=fixme

import socket
import getpass
import struct
import time
import os
from collections import deque

from PyQt5.QtWidgets import QMainWindow, \
    QApplication, \
    QWidget, \
    QVBoxLayout, \
    QHBoxLayout, \
    QTabWidget, \
    QTextEdit, \
    QLineEdit, \
    QPushButton

from PyQt5.QtCore import QThread, pyqtSignal


class MainWindow(QMainWindow):
    """This class manages the main/only window and its components."""

    # pylint: disable=too-many-instance-attributes
    # Low priority changes that would require significant refactoring
    def __init__(self):
        super().__init__()
        self.cname = 'SHIRC Client'
        self.mname = 'Meta Channel'
        self.connection = None
        self.channels = dict()
        self.nick = "UNKNOWN_USER"
        self.incoming_thread = None
        self.incoming_message_queue = deque()
        self.file_transfer = None
        self.ft_phase = 0
        self.ft_addr = None

        # Command to function "pointer" mapping
        self.commands = {
            'join': self.cmd_join,
            'leave': self.cmd_leave,
            'list': self.cmd_list,
            'nick': self.cmd_nick,
            'register': self.cmd_register,
            'send': self.cmd_send,
            'connect': self.cmd_connect,
            'disconnect': self.cmd_disconnect,
            'accept': self.cmd_accept,
        }

        # Make it pretty
        self.setWindowTitle('SHIRC Chat')
        self.setGeometry(300, 300, 800, 600)

        # Create main display widget
        container = QWidget()
        self.setCentralWidget(container)

        # Create main layout
        layout = QVBoxLayout()

        # Top row will only contain the tabs for each channel
        top_row = QHBoxLayout()
        self.channels_tabs = QTabWidget()
        self.add_channel(self.mname)
        top_row.addWidget(self.channels_tabs)

        # Bottom row contains command input and send button
        bottom_row = QHBoxLayout()
        self.cmd_input = QLineEdit('/connect 127.0.0.1 9997')

        self.send_btn = QPushButton('Send')
        self.send_btn.clicked.connect(self.parse_command)

        # So that pressing enter results in the same action as button click
        self.cmd_input.returnPressed.connect(self.send_btn.click)

        bottom_row.addWidget(self.cmd_input)
        bottom_row.addWidget(self.send_btn)

        # Put top and bottom in the main layout
        layout.addLayout(top_row)
        layout.addLayout(bottom_row)

        # Set the central container widget to have the main layout
        container.setLayout(layout)

    def add_channel(self, channel_name):
        """Instantiates a new channel and adds a tab to the main window to display it."""
        new_channel = Channel(channel_name)
        self.channels[channel_name] = new_channel
        self.channels_tabs.addTab(new_channel.display, new_channel.name)

    def parse_incoming(self, data):
        """Parses data coming from the server"""
        # pylint: disable=too-many-locals
        msg = data.popleft()
        print("Recieved data from server: {}".format(msg))
        if not msg:
            print("data len 0")
            self.connection.close()
            self.connection = None
            return

        cmd = msg[0]
        subcmd = msg[1]
        contents = msg[2:].decode('utf-8')

        print(msg)
        if cmd == 1:
            split_contents = contents.split()
            print("PRIVMSG {}".format(contents))
            chan = split_contents[0]
            source_user = split_contents[1]
            message = ' '.join(split_contents[2:])
            self.channels[chan].add_msg(source_user, message)

        if cmd == 2:
            print("Identified change nick")
            if subcmd == 2:
                print("subcmd 2")
                split_contents = contents.split()
                print(split_contents[2], self.nick)
                if split_contents[2] == self.nick:
                    self.nick = split_contents[3]
                    self.channels[split_contents[0]]\
                        .add_msg(split_contents[1], '{} is now known as {}'
                                 .format(split_contents[2], split_contents[3]))

        if cmd == 3:
            self.channels[self.mname].add_msg('SERVER', contents)

        if cmd == 9:
            if subcmd == 2:
                self.channels[self.mname].add_msg(
                    self.cname, 'File transfer request from {}, Accept?')
            self.ft_phase += 1
            print(contents)
            self.ft_addr = contents
            ft_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            split_contents = contents.split()
            addr = (split_contents[1], int(split_contents[2]))

            filename = split_contents[0].split('/')[-1]
            file = open(filename, "wb")

            ft_socket.connect(addr)
            bytes_expected = int(split_contents[3])
            bytes_transferred = 0
            while bytes_transferred < bytes_expected:
                data = ft_socket.recv(4096)
                bytes_transferred += len(data)
                file.write(data)
                print(bytes_transferred)

    def parse_command(self):
        """Parses user input and executes command if necessary"""
        command = self.cmd_input.text()
        self.cmd_input.clear()
        if not command:
            return

        split_command = command.split()
        channel_name = self.channels_tabs.tabText(
            self.channels_tabs.currentIndex())

        # / indicates a command
        if split_command[0][0] == '/':
            print('Found command: ', split_command[0], 'with parameters:',
                  split_command[1:])
            cmd_name = split_command[0][1:].lower()

            # To add a new command define the function as cmd_whatever
            # and add it to self.commands dictionary
            self.commands.get(cmd_name, self.cmd_unknown)(split_command[1:])

        # Case where user input was not a command.
        else:
            print('Sending to {name} the message {msg}'.format(
                name=channel_name, msg=command))
            self.cmd_privmsg(channel_name, command)

    def cmd_unknown(self, cmd_params):
        """This function is called if the user enters a command that is not valid."""
        # pylint: disable=unused-argument
        self.channels[self.mname].add_msg(self.cname, 'Unknown Command')

    def cmd_killserver(self, cmd_params):
        """This function issues command type 24 to the server, telling it to kill itself."""
        # pylint: disable=unused-argument
        if self.connection:
            header = struct.pack('BB', 24, 1)
            self.connection.send(header)

    def cmd_connect(self, cmd_params):
        """This function connects to a server and initializes a thread to watch the connection
        for incoming data."""
        if self.connection:
            # TODO: add currently connected addr to message
            self.channels[self.mname].add_msg(self.cname,
                                              'Already connected to a server')
            return
        self.channels[self.mname].add_msg(
            self.cname, 'Attempting to connect to {}:{}'.format(
                cmd_params[0], cmd_params[1]))
        self.connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            # Attempt TCP handshake
            self.connection.connect((cmd_params[0], int(cmd_params[1])))

            # Initialize thread to watch for incoming messages
            self.incoming_thread = ClientThread(self)

            # Link the QThread signal with parse_incoming
            self.incoming_thread.signal.connect(
                lambda: self.parse_incoming(self.incoming_message_queue))

            # Begin thread execution
            self.incoming_thread.start()

        except OSError as err:
            self.channels[self.mname].add_msg(
                self.cname, 'Unable to connect - {}'.format(err))

            # Failed so kill the thread and clear the connection
            self.incoming_thread = None
            self.connection = None
            return

        self.channels[self.mname].add_msg(self.cname, 'Connected.')
        print("Sending initial nick change")
        self.cmd_nick(getpass.getuser())
        self.nick = getpass.getuser()

    def cmd_disconnect(self, cmd_params):
        """This function disconnects from a currently connected server."""
        # pylint: disable=unused-argument
        if self.connection:
            # TODO: let server know we're disconnecting so that
            # it can remove the user from channels and such
            self.connection.close()
        else:
            self.channels[self.mname].add_msg(self.cname,
                                              'No connection to close...')

    def cmd_list(self, cmd_params):
        """This function requests a list of all currently active channels from the server."""
        # pylint: disable=unused-argument
        self.channels[self.mname].add_msg(self.cname,
                                          'Requesting list of channels...')

    def cmd_join(self, cmd_params):
        """This function joins a channel."""
        self.channels[self.mname].add_msg(
            self.cname, 'Joining channel \'{}\''.format(cmd_params[0]))
        self.add_channel(cmd_params[0])
        header = struct.pack('BB', 4, 1)
        message = header + '{}'.format(''.join(cmd_params)).encode('utf-8')
        self.connection.send(message)

    def cmd_leave(self, cmd_params):
        """This function leaves a channel."""
        self.channels[self.mname].add_msg(
            self.cname, 'Leaving channel \'{}\''.format(cmd_params[0]))

    def cmd_nick(self, new_nick):
        """This function sends a request to the server to change nicks."""
        # TODO: validate nick length and extra command parameters
        header = struct.pack('BB', 2, 1)
        message = header + '{}'.format(new_nick).encode('utf-8')
        print(message)
        self.connection.send(message)

    def cmd_register(self, cmd_params):
        """This function registers a nick with the server."""
        if len(cmd_params) < 2:
            print("Register command missing parameters.")
            self.channels[self.mname].add_msg(
                self.cname, 'Register command requires password parameter.')
            return
        self.channels[self.mname].add_msg(
            self.cname,
            'Attempting to register \'{}\' with password \'{}\''.format(
                self.nick, cmd_params[0]))
        header = struct.pack('BB', 3, 1)
        message = header + '{}'.format(cmd_params[0]).encode('utf-8')
        self.connection.send(message)

    def cmd_send(self, cmd_params):
        """This function initiates a send file."""
        if len(cmd_params) < 2:
            print("Send command missing parameters.")
            self.channels[self.mname].add_msg(
                self.cname,
                'Send command requires target user AND path to file.')
            return
        self.channels[self.mname].add_msg(
            self.cname,
            'Notifying {} of transfer request.'.format(cmd_params[0]))

        if not self.connection:
            self.channels[self.mname].add_msg(
                self.cname,
                'Unable to send file when not connected to a server.')

        # Instantiate QThread first so we can get the IP and port
        self.file_transfer = FileTransferThread(cmd_params[1])
        self.file_transfer.start()
        print(self.connection.getsockname())
        statinfo = os.stat(cmd_params[1])

        header = struct.pack('BB', 9, 1)
        message = header + cmd_params[0].encode('utf-8') + \
                  ' '.encode('utf-8') + \
                  cmd_params[1].encode('utf-8') + \
                  ' '.encode('utf-8') + \
                  self.connection.getsockname()[0].encode('utf-8') + \
                  ' '.encode('utf-8') + \
                  str(self.file_transfer.sock_name()[1]).encode('utf-8') + \
                  ' '.encode('utf-8') + \
                  str(statinfo.st_size).encode('utf-8')
        self.connection.send(message)

    def cmd_accept(self, cmd_params):
        """This function accepts/initializes a file transfer that has been requested."""
        # Check to see if theres

    def cmd_privmsg(self, target, msg):
        """This function sends a message to a channel or user."""
        if not self.connection:
            self.channels[target].add_msg(
                self.cname, "Unable to send, not connected to server.")
            return

        if target == self.mname:
            # Actually sending a message to the meta channel makes no sense.
            return
        header = struct.pack('BB', 1, 1)
        message = header + '{} {}\r\n'.format(target, msg).encode('utf-8')
        print(message)

        self.connection.send(message)


class Channel:
    """This class constitutes a single channel/tab in the main window."""

    def __init__(self, name):
        self._name = name
        self.display = QTextEdit()
        self.display.setFontFamily('Monospace')
        self.display.setReadOnly(True)
        self.messages = []

    @property
    def name(self):
        """Getter for channel name attribute."""
        return self._name

    @name.setter
    def name(self, name):
        """Setter for channel name attribute."""
        self._name = name

    # Method to record messages sent in a channel
    def add_msg(self, msg_source, msg_content):
        """Adds a message to a channel for display."""
        self.messages.append('{:>14} : {}'.format(msg_source, msg_content))
        self.display.setText('\n'.join(self.messages))


class FileTransferThread(QThread):
    """This class threads out a file transfer"""

    def __init__(self, filename):
        QThread.__init__(self)
        self.filename = filename
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind(('', 0))

    def run(self):
        """This function sends the file to the remote end."""
        # pylint: disable=unused-variable
        self.sock.listen()
        conn, client = self.sock.accept()
        file_to_read = open(self.filename, "rb")
        data = file_to_read.read()
        file_to_read.close()
        conn.sendall(data)

    def sock_name(self):
        """Helper function to get the ip and port we're listening on,
        gets sent to other end via SHIRC protocol
        """
        return self.sock.getsockname()


class ClientThread(QThread):
    """This class is a thread that monitors an open connection for incoming data to parse."""
    signal = pyqtSignal()

    def __init__(self, window):
        QThread.__init__(self)
        self.window = window
        self.connection = window.connection

    # TODO: make this hte function thats called when the client connects
    def run(self):
        """This is the function that the connection monitoring thread executes."""
        while True:
            if not self.connection:
                time.sleep(.1)
                continue
            data = self.connection.recv(512)
            print("emitting signal")
            self.window.incoming_message_queue.append(data)
            self.signal.emit()
            #self.window.parse_incoming(data)


def main():
    """Main instantiates the client and displays it on the screen."""
    client = QApplication([])
    main_window = MainWindow()
    main_window.show()
    client.exec_()


if __name__ == "__main__":
    main()
